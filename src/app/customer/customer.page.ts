import { Component, OnInit } from '@angular/core';
import { Customer } from '../models/customer.model';
import { ApiService, paramsInterface } from '../services/api/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: ['./customer.page.scss'],
})
export class CustomerPage implements OnInit {
  customer: Customer;
  id_customer: number = 0;
  load: boolean = false;
  constructor(
    public ApiService: ApiService,
    private route: ActivatedRoute
  ) {
    this.customer = new Customer(this.ApiService);
    /**
     * recogemos el parámetro id_customer y cargamos el cliente
     */
    this.route.params.subscribe(params => {
      this.id_customer = parseInt(params['id_customer']);
      this.loadCustomer();
    });
  }

  /**
   * cargamos el cliente junto con los datos de su grupo de clientes asignado
   */
  async loadCustomer() {
    let params: paramsInterface[] = [];
    params.push(
      {
        param: 'embed',
        value: 'customer_group'
      }
    );
    await this.customer.findById(this.id_customer, params);
    this.load = true;
  }

  ngOnInit() {
  }

}
