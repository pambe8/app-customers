import { ApiService, paramsInterface, resultListInterface } from '../services/api/api.service';
import { BaseModel } from './base.model';
import { CustomerGroup } from './customer-group.model';

export class Customer extends BaseModel implements InterfaceCustomer {
    id: number;
    name: string;
    description: string;
    id_customer_group: number;
    customer_group?: CustomerGroup;
    constructor(ApiService: ApiService, params?: {
        data?: any
    }) {
        super(ApiService);
        /**
         * Inicializamos la clase con los datos recibidos
         */
        this.loadData(params);

    }

    /**
     * Método para iniciar las propiedades de clase con los datos recibidos
     * @param params 
     */
    private loadData(params?: {
        data?: any
    }) {
        if (params) {
            if (params.data !== undefined) {
                this.id = params.data.id;
                this.name = params.data.name;
                this.description = params.data.description;
                this.id_customer_group = parseInt(params.data.id_customer_group);
                if (params.data.customer_group !== undefined) {
                    this.customer_group = new CustomerGroup(this.getApiService(), { data: params.data.customer_group });
                }
                if (this.id !== undefined) { //se indica que el elemento existe
                    this.setExists(true);
                }
            }
        }
    }

    /**
     * Buscar un cliente por un id
     * @param id 
     * @param params 
     */
    public findById(
        id: number,
        params?: paramsInterface[]
    ) {
        return new Promise((resolve, reject) => {
            if (params === undefined) params = [];
            this.getApiService().get({
                url: `customers/${id}`,
                params: params
            }).then(data => {
                if (data) {
                    if (data['status']) {
                        let element = data['element'];
                        this.loadData({ data: element });
                    }
                }
                resolve(true);
            });
        });
    }

    /**
     * Guardar los datos de un cliente
     */
    public insert() {
        let data: InterfaceCustomer = {
            id: this.id,
            name: this.name,
            description: this.description,
            id_customer_group: this.id_customer_group
        };
        return new Promise((resolve, reject) => {
            if (this.getExists()) { //Actualizar el cliente
                this.getApiService().put({
                    url: `customers/${this.id}`,
                    data: data
                }).then(data => {
                    if (data && data['status']) {
                        let element = data['element'];
                        this.loadData({ data: element });
                        resolve(data['status']);
                    } else {
                        resolve(false);
                    }
                });
            } else { //Crear un nuevo cliente
                this.getApiService().post({
                    url: `customers`,
                    data: data
                }).then(data => {
                    if (data && data['status']) {
                        let element = data['element'];
                        this.loadData({ data: element });
                        resolve(data['status']);
                    } else {
                        resolve(false);
                    }
                });
            }
        });
    }

    /**
     * Eliminar el cliente
     */
    public delete() {
        return new Promise((resolve, reject) => {
            if (this.getExists()) {
                this.getApiService().delete({
                    url: `customers/${this.id}`,
                }).then(data => {
                    if (data && data['status']) {
                        this.setExists(false);
                        resolve(data['status']);
                    } else {
                        resolve(false);
                    }
                });
            } else {
                resolve(false);
            }
        });
    }

    /**
     * Recuperar el listado de clientes
     * @param params 
     */
    public getCustomers(
        params?: paramsInterface[]
    ): Promise<resultListInterface> {
        return new Promise((resolve, reject) => {
            if (params === undefined) params = [];
            this.getApiService().get({
                url: `customers`,
                params: params
            }).then(data => {
                let result: resultListInterface = {
                    data: [],
                    limit: 0,
                    page: 0,
                    total: 0
                }
                if (data) {
                    let listElements: Customer[] = [];
                    if (data['status']) {
                        let list: any[] = data['data'];
                        for (let element of list) {
                            let obj = new Customer(this.getApiService(), { data: element });
                            listElements.push(obj);
                        }
                    }
                    result = {
                        data: listElements,
                        limit: data['limit'],
                        page: data['page'],
                        total: data['total']
                    }
                }

                resolve(result);
            });
        });
    }


}

/**
 * interface de cliente
 */
export interface InterfaceCustomer {
    id: number;
    name: string;
    description: string;
    id_customer_group: number;
}