import { ApiService, paramsInterface, resultListInterface } from '../services/api/api.service';
import { BaseModel } from './base.model';

export class CustomerGroup extends BaseModel implements InterfaceCustomerGroup {
    id: number;
    name: string;
    constructor(ApiService: ApiService, params?: {
        data?: any
    }) {
        super(ApiService);
        /**
         * Inicializamos la clase con los datos recibidos
         */
        this.loadData(params);

    }

    /**
     * Método para iniciar las propiedades de clase con los datos recibidos
     * @param params 
     */
    private loadData(params?: {
        data?: any
    }) {
        if (params) {
            if (params.data !== undefined) {
                this.id = params.data.id;
                this.name = params.data.name;
                if (this.id !== undefined) {
                    this.setExists(true);
                }
            }
        }
    }

    /**
     * Buscar un grupo por un id
     * @param id 
     * @param params 
     */
    public findById(
        id: number,
        params?: paramsInterface[]
    ) {
        return new Promise((resolve, reject) => {
            if (params === undefined) params = [];
            this.getApiService().get({
                url: `customer-groups/${id}`,
                params: params
            }).then(data => {
                if (data) {
                    if (data['status']) {
                        let element = data['element'];
                        this.loadData({ data: element });
                    }
                }
                resolve(true);
            });
        });
    }

    /**
     * Guardar los datos de un grupo
     */
    public insert() {
        let data: InterfaceCustomerGroup = {
            id: this.id,
            name: this.name
        };
        return new Promise((resolve, reject) => {
            if (this.getExists()) { //modificar el grupo
                this.getApiService().put({
                    url: `customer-groups/${this.id}`,
                    data: data
                }).then(data => {
                    if (data && data['status']) {
                        let element = data['element'];
                        this.loadData({ data: element });
                        resolve(data['status']);
                    } else {
                        resolve(false);
                    }
                });
            } else { //Crear un nuevo grupo
                this.getApiService().post({
                    url: `customer-groups`,
                    data: data
                }).then(data => {
                    if (data && data['status']) {
                        let element = data['element'];
                        this.loadData({ data: element });
                        resolve(data['status']);
                    } else {
                        resolve(false);
                    }
                });
            }
        });
    }

    /**
     * Eliminar el grupo
     */
    public delete() {
        return new Promise((resolve, reject) => {
            if (this.getExists()) {
                this.getApiService().delete({
                    url: `customer-groups/${this.id}`,
                }).then(data => {
                    if (data && data['status']) {
                        this.setExists(false);
                        resolve(data['status']);
                    } else {
                        resolve(false);
                    }
                });
            } else {
                resolve(false);
            }
        });
    }

    /**
     * Recuperar el listado de grupos de clientes
     * @param params 
     */
    public getCustomerGroups(
        params?: paramsInterface[]
    ): Promise<resultListInterface> {
        return new Promise((resolve, reject) => {
            if (params === undefined) params = [];
            this.getApiService().get({
                url: `customer-groups`,
                params: params
            }).then(data => {
                let result: resultListInterface = {
                    data: [],
                    limit: 0,
                    page: 0,
                    total: 0
                }
                if (data) {
                    let listElements: CustomerGroup[] = [];
                    if (data['status']) {
                        let list: any[] = data['data'];
                        for (let element of list) {
                            let obj = new CustomerGroup(this.getApiService(), { data: element });
                            listElements.push(obj);
                        }
                    }
                    result = {
                        data: listElements,
                        limit: data['limit'],
                        page: data['page'],
                        total: data['total']
                    }
                }

                resolve(result);
            });
        });
    }


}

/**
 * Interface de grupo de clientes
 */
export interface InterfaceCustomerGroup {
    id: number;
    name: string;
}