import { ApiService } from '../services/api/api.service';

export class BaseModel {
    private exists: boolean = false;
    private ApiService: ApiService;
    /**
     * iniciamos la clase con el ApiService recibido
     * @param ApiService 
     */
    constructor(ApiService: ApiService) {
        this.ApiService = ApiService;
    }
    /**
     * método para saber si existe un elemento
     */
    public getExists() {
        return this.exists;
    }
    /**
     * método para decir si existe o no un elemento
     * @param exists 
     */
    protected setExists(exists: boolean) {
        this.exists = exists;
    }
    /**
     * devolvermos el ApiService
     */
    protected getApiService() {
        return this.ApiService;
    }

}
