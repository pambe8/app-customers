import { Injectable, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class TranslationManagementService {
  private languages: string[] = [
    "es",
    "en"
  ];
  constructor(private translate: TranslateService, @Inject(DOCUMENT) private _document: any) {
    translate.addLangs(this.languages);
    translate.setDefaultLang(this.languages[0]);
    if (localStorage.getItem("lang") !== null) { //en el caso de que el usuario haya elegido un idioma anteriormente se iniciará ese idioma por defecto
      this.setLang(localStorage.getItem("lang"));
    } else { //iniciar el idioma por defecto del navegador
      let browserLang = translate.getBrowserLang();
      this.setLang(browserLang);
    }
  }
  /**
   * cambiar idioma
   * @param lang 
   */
  setLang(lang: string) {
    if (this.languages.indexOf(lang) !== -1) {
      this.translate.use(lang);
    } else {
      this.translate.use(this.languages[0]);
    }
    this._document.documentElement.lang = this.translate.currentLang;
    localStorage.setItem('lang', this.translate.currentLang);
  }

  /**
   * devolver el idioma actual
   */
  getLang() {
    return this.translate.currentLang;
  }
}
