import { TestBed } from '@angular/core/testing';

import { TranslationManagementService } from './translation-management.service';

describe('TranslationManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TranslationManagementService = TestBed.get(TranslationManagementService);
    expect(service).toBeTruthy();
  });
});
