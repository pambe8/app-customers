import { Injectable, ApplicationRef } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { interval, concat } from 'rxjs';
import { first } from 'rxjs/operators';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  constructor(public appRef: ApplicationRef, public updates: SwUpdate) {
    if (updates.isEnabled) {
      // Permitir que la aplicación se estabilice antes de utilizar `interval()`.
      const appIsStable$ = appRef.isStable.pipe(first(isStable => isStable === true));
      const everySixHours$ = interval(6 * 60 * 60);
      const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);
      //cada 21 segundos y medio se comprobará si existe una actualización en el servidor
      everySixHoursOnceAppIsStable$.subscribe(() => updates.checkForUpdate());
    }
  }
  /**
   * comprobar si existe alguna actualización en el servidor
   */
  public checkForUpdates(): void {
    this.updates.available.subscribe(event => {
      console.log('updates detected!!!!', event);
      this.toUpdate();
    });
  }

  /**
   * comenzar la actualización
   */
  isUpdating = new Subject();
  private toUpdate(): void {
    this.isUpdating.next(true);
    console.log('updating.....');
    this.updates.activateUpdate().then(() => {
      this.isUpdating.next(false);
      console.log('reload....');
      document.location.reload();
    });
  }

}
