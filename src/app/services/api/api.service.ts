import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { retryWhen, concatMap, delay, first } from 'rxjs/operators';
import { Subject, of, interval, concat, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public baseUrl: string = environment.baseUrl; //url del apirest
  constructor(
    private http: HttpClient
  ) {

  }

  /**
   * se permite detectar si se está realizando alguna petición al servidor por medio de un contador. 
   * Si el contador está a 0 es que no existen peticiones cargándose. Para informar utilizamos un observable
   */
  public isLoad = new Subject();
  private load: number = 0;
  private setDataChangeLoad(load: boolean) {
    if (load) {
      this.load++;
    } else {
      this.load--;
      if (this.load < 0) this.load = 0;
    }
    this.isLoad.next(this.load > 0);
  }

  /**
   * Se informa mediante un observable del error al realizar una petición
   */
  public isError = new Subject();
  private setDataChangeError(error: any) {
    this.isError.next(error);
  }


  /**
   * Mediante un observable indicamos si actualmente existe conexión con el servidor (se considera que no hay conexión después de 10 intentos).
   * Si con un intento se detecta que hay conexión informamos de que existe conexión
   */
  public isConnection = new Subject();
  private totalNotConnections: number = 0;
  private setConnection(isConnection: boolean) {
    if (isConnection) {
      if (this.totalNotConnections > 10) {
        this.isConnection.next(true);
      }
      this.totalNotConnections = 0;
    } else {
      this.totalNotConnections++;
      if (this.totalNotConnections > 10) {
        this.isConnection.next(false);
      }
    }

  }

  /**
   * Peticiones get
   * @param options 
   */
  public get(options: {
    url: string,
    params?: paramsInterface[]
  }) {
    options.params = options.params !== undefined ? options.params : [];
    return new Promise((resolve, reject) => {
      this.setDataChangeLoad(true);
      let headers = new HttpHeaders({
        "Accept": "application/json"
      });
      let params = new HttpParams();

      for (let i = 0; i < options.params.length; i++) {
        params = params.set(options.params[i].param, String(options.params[i].value));
      }

      let suscription = this.http.get(this.baseUrl + options.url, { headers, params }).pipe(retryWhen(errors => errors
        .pipe(
          concatMap((error, count) => {
            if ((error.status == 400 || error.status == 504 || error.status == 0)) {
              this.setConnection(false);
              return of(error.status);
            }
            return throwError(error);
          }),
          delay(1000)
        )
      )).subscribe(res => {
        this.setConnection(true);
        this.setDataChangeLoad(false);
        suscription.unsubscribe();
        resolve(res);
      },
        error => {
          this.setConnection(true);
          this.setDataChangeLoad(false);
          this.setDataChangeError(error);
          suscription.unsubscribe();
          resolve(false);
        });

    });
  }

  /**
   * Peticiones post
   * @param options 
   */
  public post(options: {
    url: string,
    data: any
  }) {
    return new Promise((resolve, reject) => {
      this.setDataChangeLoad(true);
      let body = JSON.stringify(options.data);
      let headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      });
      let suscription = this.http.post(this.baseUrl + options.url, body, { headers }).pipe(retryWhen(errors => errors
        .pipe(
          concatMap((error, count) => {
            if ((error.status == 400 || error.status == 504 || error.status == 0)) {
              this.setConnection(false);
              return of(error.status);
            }
            return throwError(error);
          }),
          delay(1000)
        )
      )).subscribe(res => {
        this.setConnection(true);
        this.setDataChangeLoad(false);
        suscription.unsubscribe();
        resolve(res);
      },
        error => {
          this.setConnection(true);
          this.setDataChangeLoad(false);
          this.setDataChangeError(error);
          suscription.unsubscribe();
          if (error.status == 401 && options.url == 'refresh-token') {
            reject(false);
          } else {
            resolve(false);
          }
        });
    });
  }

  /**
   * Peticiones put
   * @param options 
   */
  public put(options: {
    url: string,
    data: any
  }) {
    return new Promise((resolve, reject) => {
      this.setDataChangeLoad(true);
      let body = JSON.stringify(options.data);
      let headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      });
      let suscription = this.http.put(this.baseUrl + options.url, body, { headers }).pipe(retryWhen(errors => errors
        .pipe(
          concatMap((error, count) => {
            if ((error.status == 400 || error.status == 504 || error.status == 0)) {
              this.setConnection(false);
              return of(error.status);
            }
            return throwError(error);
          }),
          delay(1000)
        )
      )).subscribe(res => {
        this.setConnection(true);
        this.setDataChangeLoad(false);
        suscription.unsubscribe();
        resolve(res);
      },
        error => {
          this.setConnection(true);
          this.setDataChangeLoad(false);
          this.setDataChangeError(error);
          suscription.unsubscribe();
          resolve(false);
        });
    });
  }

  /**
   * Peticiones delete
   * @param options 
   */
  public delete(options: {
    url: string,
    data?: any
  }) {
    return new Promise((resolve, reject) => {
      this.setDataChangeLoad(true);
      let body = JSON.stringify({});
      if (options.data) body = JSON.stringify(options.data);
      let headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Accept": "application/json"
      });
      const httpOptions = {
        headers: headers,
        body: body
      };

      let suscription = this.http.delete(this.baseUrl + options.url, httpOptions).pipe(retryWhen(errors => errors
        .pipe(
          concatMap((error, count) => {
            if ((error.status == 400 || error.status == 504 || error.status == 0)) {
              this.setConnection(false);
              return of(error.status);
            }
            return throwError(error);
          }),
          delay(1000)
        )
      )).subscribe(res => {
        this.setConnection(true);
        this.setDataChangeLoad(false);
        suscription.unsubscribe();
        resolve(res);
      },
        error => {
          this.setConnection(true);
          this.setDataChangeLoad(false);
          this.setDataChangeError(error);
          suscription.unsubscribe();
          resolve(false);
        });
    });
  }

}

/**
 * interface de parametro get del api
 */
export interface paramsInterface {
  param: string,
  value: string | number
}

/**
 * interface del resultado de una lista del api
 */
export interface resultListInterface {
  data: any[],
  limit?: number,
  page?: number,
  total?: number
}