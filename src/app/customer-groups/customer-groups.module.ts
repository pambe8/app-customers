import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CustomerGroupsPageRoutingModule } from './customer-groups-routing.module';
import { CustomerGroupsPage } from './customer-groups.page';
import { TranslateModule } from '@ngx-translate/core';
import { EditCustomerGroupPageModule } from '../modals/edit-customer-group/edit-customer-group.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerGroupsPageRoutingModule,
    TranslateModule,
    EditCustomerGroupPageModule
  ],
  declarations: [CustomerGroupsPage]
})
export class CustomerGroupsPageModule {}
