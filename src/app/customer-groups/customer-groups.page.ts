import { Component, OnInit, ViewChild } from '@angular/core';
import { CustomerGroup } from '../models/customer-group.model';
import { ApiService, paramsInterface } from '../services/api/api.service';
import { TranslateService } from '@ngx-translate/core';
import { EditCustomerGroupPage } from '../modals/edit-customer-group/edit-customer-group.page';
import { ModalController, AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-customer-groups',
  templateUrl: './customer-groups.page.html',
  styleUrls: ['./customer-groups.page.scss'],
})
export class CustomerGroupsPage implements OnInit {
  load: boolean = false;
  doLoad: boolean = false;
  isSearch: boolean = false;
  sort: string = 'name';
  search: string = '';
  paginator = {
    total: 0,
    limit: 20,
    page: 0
  }
  customerGroups: CustomerGroup[] = [];
  @ViewChild('searchbar', { static: false }) searchbar;
  constructor(
    public ApiService: ApiService,
    public translate: TranslateService,
    public modalCtrl: ModalController,
    public alertController: AlertController,
    public toastController: ToastController
  ) {
    /**
     * cargamos el listado inicial de grupos de clientes
     */
    this.loadList();
  }

  /**
   * Detectamos cambios en el campo de búsqueda con un retardo de 500 milisegundos para dejar escribir al usuario antes de realizar la petición
   */
  timeSearch = null;
  getSearch(event) {
    if (this.timeSearch != null) {
      clearTimeout(this.timeSearch);
      this.timeSearch = null;
    }
    this.timeSearch = setTimeout(() => {
      this.search = event.target.value !== undefined ? event.target.value : '';
      this.restartList();
    }, 500);

  }

  /**
   * método para mostrar el campo de búsqueda
   */
  viewSearch() {
    this.isSearch = !this.isSearch;
    if (this.isSearch) {
      setTimeout(() => {
        this.searchbar.setFocus();
        this.searchbar.writeValue;
      }, 50);
    }
  }

  /**
   * método para reiniciar el listado
   */
  restartList(){
    this.paginator.page = 0;
    this.customerGroups = [];
    this.load = false;
    this.loadList();
  }

  /**
   * método encargado de cargar el listado con todos los párametros necesarios para ello
   */
  loadList() {
    return new Promise((resolve, reject) => {
      let customerGroup = new CustomerGroup(this.ApiService);
      let params: paramsInterface[] = [];
      params.push(
        {
          param: 'search',
          value: this.search
        },
        {
          param: 'sort',
          value: this.sort
        },
        {
          param: 'page',
          value: this.paginator.page
        },
        {
          param: 'limit',
          value: this.paginator.limit
        }
      );


      customerGroup.getCustomerGroups(params).then(data => {
        for (let i = 0; i < data.data.length; i++) {
          this.customerGroups.push(data.data[i]); //se asignan al final del array de grupos para poder ir avanzando en el scroll infinito
        }
        this.paginator.limit = data.limit;
        this.paginator.page = data.page;
        this.paginator.total = data.total;
        this.load = true;
        resolve(true);
      });
    });

  }

  /**
   * método que activa la acción del scroll infinito, carga la siguiente página del listado
   * @param event 
   */
  doInfinite(event) {
    this.doLoad = true;
    if (this.customerGroups.length < this.paginator.total) {
      this.paginator.page++;
      this.loadList().then(() => {
        event.target.complete();
        this.doLoad = false;
      });
    } else {
      event.target.complete();
      this.doLoad = false;
    }
  }

  /**
   * cargar el modal para añadir/editar el grupo
   * @param customerGroup 
   */
  async editCustomerGroup(customerGroup:CustomerGroup=new CustomerGroup(this.ApiService)){
    const modal = await this.modalCtrl.create({
      component: EditCustomerGroupPage,
      componentProps: { customerGroup:customerGroup }
    });
    modal.present();
    await modal.onDidDismiss();
    this.restartList();
  }

  /**
   * Eliminar un grupo
   * @param customerGroup 
   */
  async deleteCustomerGroup(customerGroup:CustomerGroup){
    const alert = await this.alertController.create({
      header: this.translate.instant('ELIMINARGRUPO', {name: customerGroup.name}),
      message: this.translate.instant('AVISOELIMINARGRUPO'),
      buttons: [
        {
          text: this.translate.instant('CANCELAR'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: this.translate.instant('ELIMINAR'),
          handler: () => {
            customerGroup.delete().then(async status=>{
              if(status){
                const toast = await this.toastController.create({
                  color:'success',
                  message: this.translate.instant('GRUPOELIMINADACORRECTAMENTE'),
                  duration: 2000
                });
                toast.present();
                this.restartList();
              }else{
                const toast = await this.toastController.create({
                  color:'danger',
                  message: this.translate.instant('ERRORALELIMINAR'),
                  duration: 2000
                });
                toast.present();
              }
              
            });
          }
        }
      ]
    });

    await alert.present();
    
  }

  ngOnInit() {
  }

}
