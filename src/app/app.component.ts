import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ApiService } from './services/api/api.service';
import { UpdateService } from './services/update/update.service';
import { TranslationManagementService } from './services/translation-management/translation-management.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  updating: boolean = false;
  isConnection: boolean = true;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private ApiService: ApiService,
    private UpdateService: UpdateService,
    private TranslationManagementService: TranslationManagementService
  ) {
    /**
     * Detectamos si se está actualizando la pwa
     */
    this.UpdateService.isUpdating.subscribe(status => {
      this.updating = <boolean>status;
    });
    /**
     * Detectamos si no se ha podido conectar al servidor al realizar una petición
     */
    this.ApiService.isConnection.subscribe(status => {
      this.isConnection = <boolean>status;
    });
    
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
