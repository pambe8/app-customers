import { Component, OnInit, Input } from '@angular/core';
import { CustomerGroup } from 'src/app/models/customer-group.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-edit-customer-group',
  templateUrl: './edit-customer-group.page.html',
  styleUrls: ['./edit-customer-group.page.scss'],
})
export class EditCustomerGroupPage implements OnInit {

  @Input() customerGroup:CustomerGroup;//recibimos el grupo
  exists:boolean=false;
  form:FormGroup=this.fb.group({//iniciamos el FormGroup 
    name: ['', [Validators.required]]
  });
  constructor(
    private modalCtrl:ModalController,
    public toastController: ToastController, 
    public translate: TranslateService,
    private fb: FormBuilder) {
    
  }
  /**
   * Guardamos los datos del grupo
   */
  save(){
    this.customerGroup.name=this.form.controls.name.value;
    this.customerGroup.insert().then(async status=>{
      if(status){
        let message=this.exists?this.translate.instant('GRUPOACTUALIZADOCORRECTAMENTE'):this.translate.instant('GRUPOCREADOCORRECTAMENTE');
        const toast = await this.toastController.create({
          color:'success',
          message: message,
          duration: 2000
        });
        toast.present();
        this.modalCtrl.dismiss();
      }else{
        const toast = await this.toastController.create({
          color:'danger',
          message: this.translate.instant('ERRORALGUARDAR'),
          duration: 2000
        });
        toast.present();
        this.modalCtrl.dismiss();
      }
    });
  }

  /**
   * Cerrar el modal
   */
  dismiss() {
    this.modalCtrl.dismiss();
  }
  ngOnInit() {
    this.exists=this.customerGroup.getExists();
    if(this.exists){//Si existe el grupo (se a cargado el modal para editar un grupo) iniciamos los campos del formulario con los datos del grupo
      this.form.controls.name.setValue(this.customerGroup.name);
    }
  }

}
