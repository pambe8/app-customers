import { Component, OnInit, Input } from '@angular/core';
import { Customer } from 'src/app/models/customer.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CustomerGroup } from 'src/app/models/customer-group.model';
import { paramsInterface, ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.page.html',
  styleUrls: ['./edit-customer.page.scss'],
})
export class EditCustomerPage implements OnInit {
  @Input() customer: Customer; //recibimos el cliente
  exists: boolean = false;
  form: FormGroup = this.fb.group({ //iniciamos el FormGroup 
    name: ['', [Validators.required]],
    id_customer_group: ['', [Validators.required]],
    description: ['']
  });
  customerGroups: CustomerGroup[] = [];
  constructor(
    private modalCtrl: ModalController,
    public toastController: ToastController,
    public translate: TranslateService,
    private fb: FormBuilder,
    private ApiService: ApiService) {

  }

  /**
   * Guardamos los datos del cliente
   */
  save() {
    this.customer.name = this.form.controls.name.value;
    this.customer.id_customer_group = this.form.controls.id_customer_group.value;
    this.customer.description = this.form.controls.description.value;
    this.customer.insert().then(async status => {
      if (status) {
        let message = this.exists ? this.translate.instant('CLIENTEACTUALIZADOCORRECTAMENTE') : this.translate.instant('CLIENTECREADOCORRECTAMENTE');
        const toast = await this.toastController.create({
          color: 'success',
          message: message,
          duration: 2000
        });
        toast.present();
        this.modalCtrl.dismiss();
      } else {
        const toast = await this.toastController.create({
          color: 'danger',
          message: this.translate.instant('ERRORALGUARDAR'),
          duration: 2000
        });
        toast.present();
        this.modalCtrl.dismiss();
      }
    });
  }

  /**
   * Cerrar el modal
   */
  dismiss() {
    this.modalCtrl.dismiss();
  }


  ngOnInit() {
    let customerGroup = new CustomerGroup(this.ApiService);
    let params: paramsInterface[] = [];
    params.push(
      {
        param: 'not_limit',
        value: 1
      }
    );
    customerGroup.getCustomerGroups(params).then(data => { //cargamos todos los grupos de clientes
      this.customerGroups = data.data;
    });


    this.exists = this.customer.getExists();
    if (this.exists) { //Si existe el cliente (se a cargado el modal para editar un cliente) iniciamos los campos del formulario con los datos del cliente
      this.form.controls.name.setValue(this.customer.name);
      this.form.controls.id_customer_group.setValue(this.customer.id_customer_group);
      this.form.controls.description.setValue(this.customer.description);
    }
  }

}
