import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotConnectionPage } from './not-connection.page';

describe('NotConnectionPage', () => {
  let component: NotConnectionPage;
  let fixture: ComponentFixture<NotConnectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotConnectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotConnectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
