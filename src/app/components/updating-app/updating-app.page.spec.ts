import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdatingAppPage } from './updating-app.page';

describe('UpdatingAppPage', () => {
  let component: UpdatingAppPage;
  let fixture: ComponentFixture<UpdatingAppPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatingAppPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdatingAppPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
