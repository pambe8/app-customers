import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'app',
    component: TabsPage,
    children: [
      {
        path: 'customers',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../customers/customers.module').then(m => m.CustomersPageModule)
          },
          {
            path: ':id_customer',
            loadChildren: () => import('../customer/customer.module').then(m => m.CustomerPageModule)
          }
        ]
      },
      {
        path: 'customer-groups',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../customer-groups/customer-groups.module').then(m => m.CustomerGroupsPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/app/customers',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/app/customers',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
