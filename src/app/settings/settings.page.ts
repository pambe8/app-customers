import { Component, OnInit } from '@angular/core';
import { TranslationManagementService } from '../services/translation-management/translation-management.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  language:string;
  constructor(public TranslationManagementService:TranslationManagementService) {
    this.language=this.TranslationManagementService.getLang();
  }
  /**
   * Detectamos el cambio del idioma por el usuario y actualizamos el idioma
   */
  changeLanguage(){
    this.TranslationManagementService.setLang(this.language);
  }

  ngOnInit() {
  }

}
